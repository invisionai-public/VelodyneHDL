from conans import ConanFile, CMake, tools

class VelodyneHDL(ConanFile):
    name = "velodyneHDL"
    lib_version="1.0.3"
    revision="1"
    version = f"{lib_version}-{revision}"
    license = "MIT"
    description = "A minimal driver for Velodyne HDL-32E/64E VLP-16 lidars"
    settings = "os", "compiler", "build_type", "arch"
    exports_sources = ["CMakeLists.txt", "*.cpp", "*.h", "*.cxx"]
    generators = "cmake"

    def source(self):
        tools.replace_in_file("CMakeLists.txt",
            "project(VelodyneHDL)",
            """project(VelodyneHDL)
               include("conanbuildinfo.cmake")
               conan_basic_setup()
            """,
            strict=True)

    def requirements(self):
        self.requires('libpcap/1.10.0')
        self.requires('boost/1.74.0')

    def build(self):
        # builds shared libraries only
        if self.settings.os != "Linux":
            raise Exception('Build only supported for Linux')
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("LICENSE", dst="licenses")
        self.copy("*.h", dst="include/velodyneHDL")
        self.copy("*.so", dst="lib", keep_path=False)

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.so*", dst="bin", src="lib")

    def package_info(self):
        self.cpp_info.libs = ["PacketBundleDecoder","PacketBundler","PacketDecoder","PacketDriver"]
