// Test custom azimuth adjusted frame feature

#include <iostream>
#include <fstream>
#include <cmath>  
#include <cassert>      

#include "PacketDecoder.h"
#include "PacketFileReader.h"

struct TestFiles
{
    const std::string pcapFile = "../../data/2021-08-12-12-20-03_Velodyne-VLP-16-Data_flatwall_2seconds_300rpm.pcap";
    const std::string correctionsFile = "../../16db.xml";
};

bool isFloatNear(float a, float b, float epsilon) {
    const auto isNear = ((a - b) < epsilon) && ((b - a) < epsilon);
    if(!isNear)
    {
      std::cout<<"[FAILED] "<<a<<" is not near "<<b<<". Epsilon = "<<epsilon<<std::endl;
    }
    return isNear;
}

bool packetCountIsNear(int a, int b, int diff)
{
  const auto isNear = ((a - b) < diff) && ((b - a) < diff);
  if(!isNear)
  {
    std::cout<<"[FAILED] "<<a<<" is not near "<<b<<". Max diff = "<<diff<<std::endl;
  }
  return isNear;  
}

int main()
{
    TestFiles testFiles;
    vtkPacketFileReader packetFileReader;

    packetFileReader.Open(testFiles.pcapFile);
    if(!packetFileReader.IsOpen())
    {
        std::cout<<"Failed to open packet file."<<std::endl;
        return -1;
    }

    // Test no adjustments
    const float origStartDeg = 0.f;
    PacketDecoder packetDecoderOrig;
    packetDecoderOrig.SetCorrectionsFile(testFiles.correctionsFile);

    int numFrames = 0;
    PacketDecoder::HDLFrame frame1;
    while(packetFileReader.IsOpen())
    {
        const unsigned char* data = nullptr;
        unsigned int dataLength = 0;
        std::chrono::time_point<std::chrono::system_clock> timeSinceStart;

        if(!packetFileReader.NextPacket(data, dataLength, timeSinceStart))
        {
          std::cout<<"Reached end of packet file."<<std::endl;
          return -1;
        }
        std::string dataStr = std::string(reinterpret_cast<char const*>(data), dataLength);

        std::string* dataPtr = &dataStr;
        unsigned* lengthPtr = &dataLength;
        packetDecoderOrig.DecodePacket(dataPtr, lengthPtr);

        if(!packetDecoderOrig.GetFrames().empty())
        {
          packetDecoderOrig.GetLatestFrame(&frame1);
          // skip first frame since it is very likely incomplete
          if(++numFrames >= 2)
          {
            break;
          }
        }
    }

    // make sure we have data for the frame
    assert(!frame1.azimuth.empty());
    
    // check that azimuth start is around 0 degrees
    const float startingDeg = frame1.azimuth.front()/100.f;
    assert(isFloatNear(origStartDeg, startingDeg, 0.5));
    
    const auto originalNumPackets = frame1.x.size();

    // Test adjustments
    const float adjustedSetStartDeg = 15.f;
    PacketDecoder packetDecoderMod(static_cast<int>(adjustedSetStartDeg));
    packetDecoderMod.SetCorrectionsFile(testFiles.correctionsFile);

    PacketDecoder::HDLFrame frame2;
    numFrames=0;
    while(packetFileReader.IsOpen())
    {
        const unsigned char* data = nullptr;
        unsigned int dataLength = 0;
        std::chrono::time_point<std::chrono::system_clock> timeSinceStart;
        if(!packetFileReader.NextPacket(data, dataLength, timeSinceStart))
        {
            std::cout<<"Reached end of packet file"<<std::endl;
            return -1;
        }
        std::string dataStr = std::string(reinterpret_cast<char const*>(data), dataLength);

        std::string* dataPtr = &dataStr;
        unsigned* lengthPtr = &dataLength;
        packetDecoderMod.DecodePacket(dataPtr, lengthPtr);
        if(!packetDecoderMod.GetFrames().empty())
        {
          packetDecoderMod.GetLatestFrame(&frame2);            
          if(++numFrames >= 2) // discard first frame since its likely not complete
          {
            break;
          }
        }
    }

    // make sure we have data for the frame
    assert(!frame2.azimuth.empty());

    const float adjustedStartingDeg = frame2.azimuth.front()/100.f;
    assert(isFloatNear(adjustedSetStartDeg, adjustedStartingDeg, 0.5));    

    // number of packets in both frames should be close since we still sweep same FOV just 
    // starting position is configurable now 
    const auto adjustedNumPackets = frame2.x.size();
    assert(packetCountIsNear(originalNumPackets, adjustedNumPackets, 10));
    
    std::cout<<"All tests passed for azimuth adjusted packet decoder!"<<std::endl;

    return 0;
}
