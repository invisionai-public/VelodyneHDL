// test new packet timestamping procedure

#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <cassert> 
#include <cstring>

#if __GNUC__ >= 8
#include <filesystem>
namespace fs = std::filesystem;
#else
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#endif     

#include "PacketFileReader.h"
#include "PacketFileWriter.h"

struct TestFiles
{
    const std::string pcapFile = "../../data/2021-08-12-12-20-03_Velodyne-VLP-16-Data_flatwall_2seconds_300rpm.pcap";
    const std::string correctionsFile = "../../16db.xml";
};


int main()
{
  TestFiles testFiles;
  vtkPacketFileReader packetFileReader;

  // first lets just get some dummy data to work with, so read in some packets from our test data 
  packetFileReader.Open(testFiles.pcapFile);
  if(!packetFileReader.IsOpen())
  {
      std::cout<<"Failed to open packet file."<<std::endl;
      return -1;
  }
  
  // get just a few packets to test with
  const size_t numOfPacketsToWrite = 5;
  std::vector<std::string> packetsToWrite;
  
  while(packetFileReader.IsOpen())
  {
    const unsigned char* data = nullptr;
    unsigned int dataLength = 0;
    std::chrono::time_point<std::chrono::system_clock> timestamp;
    
    if(!packetFileReader.NextPacket(data, dataLength, timestamp))
    {
      std::cout<<"Reached end of sample packet file."<<std::endl;
      return -1;
    }
    
    std::string dataStr = std::string(reinterpret_cast<char const*>(data), dataLength);
    packetsToWrite.push_back(std::move(dataStr));

    if(packetsToWrite.size() >= numOfPacketsToWrite) { break; }
  }
  packetFileReader.Close();
  
  // we will write a temporary pcap file with known timestamps  
  char tmpPcapFilename[] = "/tmp/pcapfileXXXXXX";
  mkstemp(tmpPcapFilename);
  std::cout<<"Writing temporary packet data to "<<tmpPcapFilename<<std::endl;
  vtkPacketFileWriter packetFileWriter;
  packetFileWriter.Open(std::string(tmpPcapFilename));
  assert(packetFileWriter.IsOpen());
  
  const auto startTime = std::chrono::high_resolution_clock::now();
  auto currentPacketTime = startTime;
  auto addToTime = std::chrono::milliseconds(101);
  for(const auto& packet : packetsToWrite)
  {
    packetFileWriter.WritePacket(reinterpret_cast<const unsigned char*>(packet.c_str()), packet.size(), currentPacketTime);
    currentPacketTime += addToTime;
  }
  
  // close this temporary file so we can read it again and make sure the timestamp values we assigned are correct 
  packetFileWriter.Close(); 
  packetFileReader.Open(tmpPcapFilename);
  
  currentPacketTime = startTime;
  size_t numPackets = 0; // make sure pcap contains expected number of packets
  while(packetFileReader.IsOpen())
  {
    const unsigned char* data = nullptr;
    unsigned int dataLength = 0;
    std::chrono::time_point<std::chrono::system_clock> timestamp;
    
    if(!packetFileReader.NextPacket(data, dataLength, timestamp))
    {
      std::cout<<"Reached end of temporary pcap file."<<std::endl;
      assert(numPackets == packetsToWrite.size());
      std::cout<<"All tests passed for timestamping packets in pcap files."<<std::endl;
      return 0;
    }
    
    // check that timestamps match expected values
    assert(std::chrono::duration_cast<std::chrono::microseconds>(currentPacketTime.time_since_epoch()).count() == 
              std::chrono::duration_cast<std::chrono::microseconds>(timestamp.time_since_epoch()).count());
  
    currentPacketTime += addToTime;
    numPackets++;
  }
  
  
  
}
