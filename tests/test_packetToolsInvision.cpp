// Test script to utilize all packet tools and check memory usage
// Script to check for leaks -> scripts/mem_check_lib.sh

#include <iostream>
#include <stdlib.h>

#include "PacketDecoder.h"
#include "PacketBundler.h"
#include "PacketBundleDecoder.h"
#include "PacketFileReader.h"
#include "PacketDriver.h"
#include "PacketFileWriter.h"

struct TestFiles
{
    const std::string pcapFile = "../../data/2021-08-12-12-20-03_Velodyne-VLP-16-Data_flatwall_2seconds_300rpm.pcap";
    const std::string correctionsFile = "../../16db.xml";
};

int main()
{
    TestFiles testFiles;
    vtkPacketFileReader packetFileReader;

    // TEST 0: packetReader
    packetFileReader.Open(testFiles.pcapFile);
    if(!packetFileReader.IsOpen())
    {
        std::cout<<"Failed to open packet file."<<std::endl;
        return -1;
    }

    // TEST 1: PacketDecoder
    PacketDecoder packetDecoder;
    packetDecoder.SetCorrectionsFile(testFiles.correctionsFile);

    while(packetFileReader.IsOpen() && packetDecoder.GetFrames().empty())
    {
        const unsigned char* data = nullptr;
        unsigned int dataLength = 0;
        std::chrono::time_point<std::chrono::system_clock> ts;
        if(!packetFileReader.NextPacket(data, dataLength, ts))
        {
            std::cout<<"Reached end of packet file"<<std::endl;
            return -1;
        }
        std::string dataStr = std::string(reinterpret_cast<char const*>(data), dataLength);

        std::string* dataPtr = &dataStr;
        unsigned* lengthPtr = &dataLength;
        packetDecoder.DecodePacket(dataPtr, lengthPtr);
    }

    PacketDecoder::HDLFrame decoderLatestFrame;
    packetDecoder.GetLatestFrame(&decoderLatestFrame);
    std::cout<<"Decoded one frame using PacketDecoder."<<std::endl;

    // TEST 2: PacketBundler & PacketBundleDecoder
    PacketBundler bundler;
    PacketBundleDecoder bundleDecoder;

    while(packetFileReader.IsOpen() && bundler.GetBundles().empty())
    {
        const unsigned char* data = nullptr;
        unsigned int dataLength = 0;
        std::chrono::time_point<std::chrono::system_clock> ts;
        if(!packetFileReader.NextPacket(data, dataLength, ts))
        {
            std::cout<<"Reached end of packet file"<<std::endl;
            return -1;
        }
        std::string dataStr = std::string(reinterpret_cast<char const*>(data), dataLength);

        bundler.BundlePacket(&dataStr, &dataLength);
    }

    std::string latestBundle;
    unsigned int latestBundleLength;
    PacketBundleDecoder::HDLFrame latestFrame;
    bundler.GetLatestBundle(&latestBundle, &latestBundleLength);
    bundleDecoder.DecodeBundle(&latestBundle, &latestBundleLength);
    bundleDecoder.GetLatestFrame(&latestFrame);
    std::cout<<"Decoded one frame using BundleDecoder."<<std::endl;

    // TEST 3: PacketDriver
    PacketDriver driver(DATA_PORT);

    std::string data;
    unsigned int dataLength;
    std::cout<<"Waiting for data packet..."<<std::endl;
    driver.GetPacket(&data, &dataLength);

    // TEST 4: packetWriter
    vtkPacketFileWriter writer;
    char tmpFilename[] = "/tmp/pcapXXXXXX";
    if(mkstemp(tmpFilename) == -1)
    {
        std::cout<<"Problem creating temp file."<<std::endl;
        return -1;
    }
    if (!writer.Open(std::string(tmpFilename)))
    {
        std::cout<<"Could not open pcap file to write"<<std::endl;
        return -1;
    }
    writer.WritePacket(reinterpret_cast<const unsigned char*>(data.c_str()), data.length(), std::chrono::system_clock::now());
    std::cout<<"Write packet to temporary file."<<std::endl;

    return 0;
}
