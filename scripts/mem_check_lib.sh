# Valgrind is tool commonly used to detect memory management
# Ref. https://valgrind.org/
# Run script to get summary of memory usage and possible leaks 

#!/bin/bash
set -e

valgrind ../build/bin/test_packetToolsInvision --leak-check=full $@